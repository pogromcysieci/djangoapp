from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    url(r'^money/settings/$', views.money_settings, name='money_settings'),

    url(r'^money/category/add/$', views.category_add, name="category_add"),
    url(r'^money/category/remove/(?P<slug>[\w-]+)/$', views.category_remove, name="category_remove"),
    url(r'^money/category/edit/(?P<slug>[\w-]+)/$', views.category_edit, name="category_edit"),


    url(r'^money/subcategory/add/$', views.subcategory_add, name="subcategory_add"),
    url(r'^money/subcategory/remove/(?P<pk>\d+)-(?P<slug>[\w-]+)/$', views.subcategory_remove, name="subcategory_remove"),
    url(r'^money/subcategory/edit/(?P<pk>\d+)-(?P<slug>[\w-]+)/$', views.subcategory_edit, name="subcategory_edit"),

    url(r'^money/list/$', views.money_list, name='money_list'),
    url(r'^money/list/add/$', views.money_list_item_add, name="money_list_item_add"),
    url(r'^money/list/remove/(?P<pk>\d+)/$', views.money_list_item_remove, name="money_list_item_remove"),
    url(r'^money/list/edit/(?P<pk>\d+)/$', views.money_list_item_edit, name="money_list_item_edit"),
    url(r'^money/analyze-monthly/$', views.money_analyze_monthly, name="money_analyze_monthly"),

    url(r'^money/export/xls/$', views.money_export_xls, name="money_export_xls"),

    url(r'^ajax/load_category/$', views.load_category, name='ajax_load_category'),
    url(r'^ajax/load_subcategory/$', views.load_subcategory, name='ajax_load_subcategory'),
    url(r'^ajax/add_category/$', views.category_add_modal, name="category_add_modal"),

    url(r'^api/data/spend-chart/$', views.SpendChartData.as_view(), name='api_spend_chart_data'),
    url(r'^api/data/category-chart/$', views.CategoryChartData.as_view(), name='api_category_chart_data'),
]

from django import forms
from .models import SpendItem, SpendType
import django_filters


class SpendItemFilter(django_filters.FilterSet):
    item_place = django_filters.CharFilter(lookup_expr='icontains')
    item_year = django_filters.CharFilter(field_name='item_date', lookup_expr='year')
    min_date = django_filters.DateTimeFilter(field_name='item_date',
                                             lookup_expr='gte')
    max_date = django_filters.DateTimeFilter(field_name='item_date',
                                             lookup_expr='lte')
    spender = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = SpendItem
        fields = ['spend_type', 'category', 'subcategory', 'item_year', 'item_place', 'spender', ]

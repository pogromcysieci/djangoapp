from django import forms

from .models import SpendCategory, SpendSubcategory, SpendType, SpendItem


class SpendCategoryForm(forms.ModelForm):

    class Meta:
        model = SpendCategory
        fields = ('spend_type', 'category_name')


class SpendSubcategoryForm(forms.ModelForm):
    spend_type = forms.ModelChoiceField(queryset=SpendType.objects.all(),
                                        required=True)

    class Meta:
        model = SpendSubcategory
        fields = ('spend_type', 'category', 'subcategory_name')

    def __init__(self, *args, **kwargs):
        super(SpendSubcategoryForm, self).__init__(*args, **kwargs)
        #self.fields['category'].queryset = SpendCategory.objects.none()


class SpendItemForm(forms.ModelForm):

    class Meta:
        model = SpendItem
        fields = ('spend_type', 'category', 'subcategory', 'item_date',
                  'item_place', 'spender', 'amount', 'currency', 'comment')

from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.http import HttpResponse, JsonResponse
from django.db.models import Sum
from .models import SpendType, SpendCategory, SpendSubcategory, SpendItem
from .forms import SpendCategoryForm, SpendSubcategoryForm, SpendItemForm
from .filters import SpendItemFilter

from rest_framework.views import APIView
from rest_framework.response import Response

import xlwt, random


def money_settings(request):
    subcategory = SpendSubcategory.objects.all().order_by("category__category_name")
    category = SpendCategory.objects.all().order_by("category_name")

    return render(request, 'settings.html', {'subcategory': subcategory, 'category': category})


def category_edit(request, slug):
    category = get_object_or_404(SpendCategory, category_slug=slug)
    if request.method == "POST":
        form = SpendCategoryForm(request.POST, instance=category)
        if form.is_valid():
            category = form.save(commit=False)
            category.created_by = request.user
            category.created_date = timezone.now()
            category.save()
            return redirect('money_settings')
    else:
        form = SpendCategoryForm(instance=category)
    return render(request, 'category_edit.html', {'form': form})


def category_add(request):
    if request.method == "POST":
        form = SpendCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.created_by = request.user
            category.created_date = timezone.now()
            category.save()
            return redirect('money_settings')
    else:
        form = SpendCategoryForm()
    return render(request, 'category_edit.html', {'form': form})


def category_remove(request, slug):
    category = get_object_or_404(SpendCategory, category_slug=slug)
    if request.method == "POST":
        category.delete()
        return redirect('money_settings')
    return render(request, 'category_remove.html', {'category': category})


def subcategory_edit(request, slug, pk):
    subcategory = get_object_or_404(SpendSubcategory, subcategory_slug=slug, pk=pk)
    if request.method == "POST":
        form = SpendSubcategoryForm(request.POST, instance=subcategory)
        if form.is_valid():
            category = form.save(commit=False)
            category.created_by = request.user
            category.created_date = timezone.now()
            category.save()
            return redirect('money_settings')
    else:
        form = SpendSubcategoryForm(instance=subcategory)
    return render(request, 'subcategory_edit.html', {'form': form})


def subcategory_add(request):
    if request.method == "POST":
        form = SpendSubcategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.created_by = request.user
            category.created_date = timezone.now()
            category.save()
            return redirect('money_settings')
    else:
        form = SpendSubcategoryForm()
    return render(request, 'subcategory_edit.html', {'form': form})


def subcategory_remove(request, slug, pk):
    subcategory = get_object_or_404(SpendSubcategory, subcategory_slug=slug, pk=pk)
    if request.method == "POST":
        subcategory.delete()
        return redirect('money_settings')
    return render(request, 'subcategory_remove.html', {'subcategory': subcategory})


def load_category(request):
    spend_type_id = request.GET.get('spend_type')
    category = SpendCategory.objects.filter(spend_type=spend_type_id)
    return render(request, 'category_dropdown_list.html', {'category': category})


def load_subcategory(request):
    category_id = request.GET.get('category')
    subcategory = SpendSubcategory.objects.filter(category=category_id)
    return render(request, 'subcategory_dropdown_list.html', {'subcategory': subcategory})


def category_add_modal(request):
    if request.method == "POST":
        form = SpendCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.created_by = request.user
            category.created_date = timezone.now()
            category.save()
            return redirect('money_settings')
    else:
        form = SpendCategoryForm()
    return render(request, 'category_edit_modal.html', {'form': form})


def money_list(request):
    spend_item_list_datalist = SpendItem.objects.order_by('item_place').values_list('item_place', flat=True).distinct()
    spend_spender_datalist = SpendItem.objects.order_by('spender').values_list('spender', flat=True).distinct()
    spend_year_datalist = SpendItem.objects.dates('item_date', 'year')
    spend_item_list = SpendItem.objects.all().order_by("-item_date")
    spend_item_filter = SpendItemFilter(request.GET, queryset=spend_item_list)
    return render(request, 'money_list.html', {'filter': spend_item_filter,
                                               'item_datalist': spend_item_list_datalist,
                                               'spender_datalist': spend_spender_datalist,
                                               'year_datalist': spend_year_datalist})


def money_list_item_add(request):
    spend_item_list_datalist = SpendItem.objects.order_by('item_place').values_list('item_place', flat=True).distinct()
    spend_spender_datalist = SpendItem.objects.order_by('spender').values_list('spender', flat=True).distinct()
    if request.method == "POST":
        form = SpendItemForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.created_by = request.user
            item.created_date = timezone.now()
            item.save()
            return redirect('money_list')
    else:
        form = SpendItemForm()
    return render(request, 'money_list_item_edit.html', {'form': form,
                                                         'item_datalist': spend_item_list_datalist,
                                                         'spender_datalist': spend_spender_datalist})


def money_list_item_edit(request, pk):
    spend_item_list_datalist = SpendItem.objects.order_by('item_place').values_list('item_place', flat=True).distinct()
    spend_spender_datalist = SpendItem.objects.order_by('spender').values_list('spender', flat=True).distinct()
    money_item = get_object_or_404(SpendItem, pk=pk)
    if request.method == "POST":
        form = SpendItemForm(request.POST, instance=money_item)
        if form.is_valid():
            item = form.save(commit=False)
            item.created_by = request.user
            item.created_date = timezone.now()
            item.save()
            return redirect('money_list')
    else:
        form = SpendItemForm(instance=money_item)
    return render(request, 'money_list_item_edit.html', {'form': form,
                                                         'item_datalist': spend_item_list_datalist,
                                                         'spender_datalist': spend_spender_datalist})


def money_list_item_remove(request, pk):
    money_item = get_object_or_404(SpendItem, pk=pk)
    if request.method == "POST":
        money_item.delete()
        return redirect('money_list')
    return render(request, 'money_list_item_remove.html', {'money_item': money_item})


def money_export_xls(request):
    spend_item_list = SpendItem.objects.all()

    spend_type = request.GET.get('spend_type')
    if spend_type:
        spend_item_list = spend_item_list.filter(spend_type=spend_type)

    category = request.GET.get('category')
    if category:
        spend_item_list = spend_item_list.filter(category=category)

    subcategory = request.GET.get('subcategory')
    if subcategory:
        spend_item_list = spend_item_list.filter(subcategory=subcategory)

    item_place = request.GET.get('item_place')
    if item_place:
        spend_item_list = spend_item_list.filter(item_place=item_place)

    item_year = request.GET.get('item_year')
    if item_year:
        spend_item_list = spend_item_list.filter(item_date__year=item_year)

    min_date = request.GET.get('min_date')
    if min_date:
        spend_item_list = spend_item_list.filter(item_date__gt=min_date)

    max_date = request.GET.get('max_date')
    if max_date:
        spend_item_list = spend_item_list.filter(item_date__lt=max_date)

    spender = request.GET.get('spender')
    if spender:
        spend_item_list = spend_item_list.filter(spender=spender)

    spend_item_list = spend_item_list.order_by("-item_date")

    response = HttpResponse(content_type='text/ms-excel')
    filename = "money-data"
    response['Content-Disposition'] = 'attachment; filename={0}.xls'.format(filename)

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Consumptions")

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    date_format = xlwt.XFStyle()
    date_format.num_format_str = 'dd-mm-yyyy'

    columns = ['Data', 'Miejsce', 'Kwota', 'Waluta', 'Kto', 'Typ', 'Kategoria', 'Podkategoria', 'Komentarz']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    for i in spend_item_list:
        row_num += 1
        row = [i.item_date, i.item_place, i.amount, i.currency, i.spender, i.spend_type.spend_type,
               i.category.category_name, i.subcategory.subcategory_name, i.comment]
        for col_num in range(len(row)):
            if col_num == 0:
                ws.write(row_num, col_num, row[col_num], date_format)
            else:
                ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def money_analyze_monthly(request):
    spend_amount = SpendType.objects.annotate(total_amount=Sum('spenditem__amount'))
    spend_wydatki = SpendItem.objects.filter(spend_type__spend_type="Wydatki").aggregate(sum=Sum('amount'))
    spend_przychody = SpendItem.objects.filter(spend_type__spend_type="Przychody").aggregate(sum=Sum('amount'))
    spend_difference = spend_przychody['sum'] - spend_wydatki['sum']
    spend_participation = spend_wydatki['sum'] / spend_przychody['sum'] * 100
    spend_participation = round(spend_participation, 2)

    category_amount = SpendCategory.objects.annotate(total_amount=Sum('spenditem__amount')).order_by('-total_amount')

    return render(request, 'money_analyze_monthly.html', {'spend_amount': spend_amount,
                                                          'spend_difference': spend_difference,
                                                          'spend_participation': spend_participation,
                                                          'category_amount': category_amount,
                                                          "customers": 10})


def colors(n):
    ret = []
    r = int(random.random() * 256)
    g = int(random.random() * 256)
    b = int(random.random() * 256)
    step = 256 / n
    for i in range(n):
        r += step
        g += step
        b += step
        r = int(r) % 256
        g = int(g) % 256
        b = int(b) % 256
        ret.append((r, g, b))
    return ret


class CategoryChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        labels = SpendCategory.objects.annotate(total_amount=Sum('spenditem__amount'))\
                                      .order_by('-total_amount').values_list("category_name")
        default_items = SpendCategory.objects.annotate(total_amount=Sum('spenditem__amount'))\
                                      .order_by('-total_amount').values_list("total_amount")
        background_color = []
        border_color = []
        data = {
            "labels": labels,
            "default": default_items,
            "backgroundColor": background_color,
            "borderColor": border_color
        }
        return Response(data)


class SpendChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        labels = SpendType.objects.annotate(total_amount=Sum('spenditem__amount'))\
                                  .order_by('-total_amount').values_list("spend_type")
        default_items = SpendType.objects.annotate(total_amount=Sum('spenditem__amount'))\
                                 .order_by('-total_amount').values_list("total_amount")
        data = {
            "labels": labels,
            "default": default_items,
        }
        return Response(data)


from django.contrib import admin
from .models import SpendType, SpendCategory, SpendSubcategory, SpendItem


admin.site.register(SpendType)
admin.site.register(SpendCategory)
admin.site.register(SpendSubcategory)
admin.site.register(SpendItem)

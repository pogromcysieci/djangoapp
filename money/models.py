from django.db import models
from django.utils import timezone
from django.template.defaultfilters import slugify
import datetime


class SpendType(models.Model):
    spend_type = models.CharField(max_length=50)
    spend_type_slug = models.SlugField(blank=True,
                                       null=True)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.spend_type

    def save(self, *args, **kwargs):
        self.spend_type_slug = slugify(self.spend_type)
        super(SpendType, self).save(*args, **kwargs)


class SpendCategory(models.Model):
    category_name = models.CharField(max_length=50)
    category_slug = models.SlugField(blank=True,
                                     null=True)
    spend_type = models.ForeignKey(SpendType, on_delete=models.CASCADE)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.category_name

    def save(self, *args, **kwargs):
        self.category_slug = slugify(self.category_name)
        super(SpendCategory, self).save(*args, **kwargs)


class SpendSubcategory(models.Model):
    subcategory_name = models.CharField(max_length=50)
    subcategory_slug = models.SlugField(blank=True,
                                        null=True)
    category = models.ForeignKey(SpendCategory, on_delete=models.CASCADE)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.subcategory_name

    def save(self, *args, **kwargs):
        self.subcategory_slug = slugify(self.subcategory_name)
        super(SpendSubcategory, self).save(*args, **kwargs)


class SpendItem(models.Model):
    spend_type = models.ForeignKey(SpendType, on_delete=models.CASCADE)
    category = models.ForeignKey(SpendCategory, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SpendSubcategory, on_delete=models.CASCADE)
    item_date = models.DateField(default=timezone.now)
    item_place = models.CharField(max_length=255)
    spender = models.CharField(max_length=50)
    comment = models.CharField(max_length=255, blank=True, default='')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=5, default="zł")
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.item_date) + " - " + str(self.amount) + " " + self.currency + " (" + self.item_place + ")"

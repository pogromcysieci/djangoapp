from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^meters/$', views.meters, name='meters'),
    url(r'^meters/add/$', views.meter_add, name="meter_add"),
    url(r'^meters/type_add/$', views.meter_type_add, name="meter_type_add"),
    url(r'^meters/(?P<slug>[\w-]+)/pdf/(?P<year>[\w-]+)/$', views.export_measurements_pdf, name="export_pdf"),
    url(r'^meters/(?P<slug>[\w-]+)/xls/(?P<year>[\w-]+)/$', views.export_measurements_xls, name="export_xls"),
    url(r'^meters/(?P<slug>[\w-]+)/csv/(?P<year>[\w-]+)/$', views.export_measurements_csv, name="export_csv"),
    url(r'^meters/(?P<slug>[\w-]+)/$', views.meters_list, name="meters_list"),
    url(r'^meters/edit/(?P<pk>\d+)', views.meter_edit, name="meter_edit"),
    url(r'^meters/edit/(?P<slug>[\w-]+)', views.meter_type_edit, name="meter_type_edit"),
    url(r'^meters/remove/(?P<pk>\d+)/$', views.meter_remove, name="meter_remove"),
    url(r'^meters/remove/(?P<slug>[\w-]+)/$', views.meter_type_remove, name="meter_type_remove"),

]

from django.db import models
from django.utils import timezone
from django.template.defaultfilters import slugify
import datetime
import calendar


class MeterType(models.Model):
    meter_type = models.CharField(max_length=50)
    slug = models.SlugField(blank=True,
                            null=True)
    meter_description = models.CharField(max_length=200,
                                         blank=True,
                                         null=True)
    meter_id = models.CharField(max_length=50,
                                blank=True,
                                null=True)
    unit_of_measure = models.CharField(max_length=50)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.meter_type

    def save(self, *args, **kwargs):
        self.slug = slugify(self.meter_type)
        super(MeterType, self).save(*args, **kwargs)


class Measurement(models.Model):
    TODAY = datetime.date.today()
    meter_type = models.ForeignKey(MeterType, related_name='measurement', on_delete=models.CASCADE)
    state = models.DecimalField(max_digits=10, decimal_places=3)
    state_date = models.DateField(default=datetime.date(TODAY.year,
                                                        TODAY.month,
                                                        calendar.monthrange(TODAY.year, TODAY.month)[1]))
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.meter_type) + " (" + str(self.state) + "" + ") "

    def get_year(self):
        return self.state_date.year


class MeasurementConsumption(models.Model):
    def __init__(self, measurement, consumption):
        self.measurement = measurement
        self.consumption = consumption

from django.contrib import admin
from .models import MeterType, Measurement


admin.site.register(MeterType)
admin.site.register(Measurement)

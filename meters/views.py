from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.db.models import Max, Count
from .models import MeterType, Measurement, MeasurementConsumption
from .forms import MeasurementForm, MeterTypeForm

from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse

import csv
import xlwt
import datetime


def home(request):
    return render(request, 'home.html', {})


def meters(request):
    meters = MeterType.objects.all()
    measurement = Measurement.objects.values('meter_type').annotate(state_date_max=Max('state_date'),
                                                                    measurement_count=Count('meter_type'),
                                                                    state_max=Max('state'))
    return render(request, 'meters.html', {'meters': meters, 'measurement': measurement})


def meters_list(request, slug):
    meter_type = get_object_or_404(MeterType, slug=slug)
    measurements = Measurement.objects.filter(meter_type__slug=slug).order_by("state_date")

    if measurements:
        consumption = calculate_consumption(measurements)

        count_months = (consumption[0].measurement.state_date.year - consumption[-1].measurement.state_date.year) * 12 \
                       + consumption[0].measurement.state_date.month - consumption[-1].measurement.state_date.month

        if count_months > 1:
            avg = round(sum([c.consumption for c in consumption]) / count_months, 3)
        else:
            avg = 0.000

        maximum = max([c.consumption for c in consumption])

        return render(request, 'meters_list.html', {'meter_type': meter_type,
                                                    'consumption': consumption,
                                                    'avg': avg,
                                                    'maximum': maximum,
                                                    'count_months': count_months})
    else:
        return render(request, 'meters_list.html', {'meter_type': meter_type})


def calculate_consumption(measurements):
    consumption = []

    for idx, measure in enumerate(measurements):
        if idx == 0:
            consumption.append(MeasurementConsumption(measure, 0))
        else:
            consumption.append(MeasurementConsumption(measure, measure.state - measurements[idx - 1].state))

    consumption.sort(key=lambda consumption: consumption.measurement.state_date, reverse=True)

    return consumption


def meter_edit(request, pk):
    measurement = get_object_or_404(Measurement, pk=pk)
    if request.method == "POST":
        form = MeasurementForm(request.POST, instance=measurement)
        if form.is_valid():
            measurement = form.save(commit=False)
            measurement.created_by = request.user
            measurement.created_date = timezone.now()
            measurement.save()
            return redirect('meters_list', measurement.meter_type.slug)
    else:
        form = MeasurementForm(instance=measurement)
    return render(request, 'meters_edit.html', {'form': form})


def meter_add(request):
    if request.method == "POST":
        form = MeasurementForm(request.POST)
        if form.is_valid():
            measurement = form.save(commit=False)
            measurement.created_by = request.user
            measurement.created_date = timezone.now()
            measurement.save()
            return redirect('meters_list', measurement.meter_type.slug)
    else:
        form = MeasurementForm()
    return render(request, 'meters_edit.html', {'form': form})


def meter_remove(request, pk):
    measurement = get_object_or_404(Measurement, pk=pk)
    if request.method == "POST":
        measurement.delete()
        return redirect('meters_list', measurement.meter_type.slug)
    return render(request, 'meters_remove.html', {'meter_type': measurement})


def export_measurements_pdf(request, slug, year=None):
    measurements = Measurement.objects.filter(meter_type__slug=slug, state_date__year=year).order_by("state_date")
    meter_type = MeterType.objects.filter(slug=slug)[0]

    response = HttpResponse(content_type='application/pdf')
    filename = slug + "-" + year
    response['Content-Disposition'] = 'attachment; filename={0}.pdf'.format(filename)

    buffer = BytesIO()
    p = canvas.Canvas(buffer)

    # Start writing the PDF here
    p.drawString(30, 750, meter_type.meter_type)
    p.drawString(30, 735, 'S/N: ' + meter_type.meter_id)
    p.drawString(500, 750, datetime.datetime.now().strftime("%Y-%m-%d"))
    p.line(30, 730, 580, 730)
    p.drawString(100, 100, "ddd")
    # End writing

    p.showPage()
    p.save()

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)

    return response


def select_data_to_export(slug, year=None):
    year2 = str(int(year) - 1)

    measurements_current_year = Measurement.objects.filter(meter_type__slug=slug,
                                                           state_date__year=year)
    measurements_previous_year = Measurement.objects.filter(meter_type__slug=slug,
                                                            state_date__year=year2).order_by("-state_date")
    measurements_only_last_month_previous_year = Measurement.objects.filter(meter_type__slug=slug,
                                                                            state_date__year=year2,
                                                                            id=measurements_previous_year[0].id)
    measurements = measurements_current_year.union(measurements_only_last_month_previous_year).order_by("state_date")
    return measurements


def export_measurements_csv(request, slug, year=None):
    response = HttpResponse(content_type='text/csv')
    filename = slug + "-" + year
    response['Content-Disposition'] = 'attachment; filename={0}.csv'.format(filename)

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    writer.writerow(['Mater_type', 'State', 'Consumption', 'State_date'])

    measurements = select_data_to_export(slug, year)
    consumption = calculate_consumption(measurements)

    for c in consumption:
        writer.writerow([c.measurement.meter_type, c.measurement.state, c.consumption, c.measurement.state_date, ])

    return response


def export_measurements_xls(request, slug, year=None):
    response = HttpResponse(content_type='text/ms-excel')
    filename = slug + "-" + year
    response['Content-Disposition'] = 'attachment; filename={0}.xls'.format(filename)

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Consumptions")

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Mater_type', 'State', 'Consumption', 'State_date']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    measurements = select_data_to_export(slug, year)
    consumption = calculate_consumption(measurements)
    font_style = xlwt.XFStyle()

    for c in consumption:
        row_num += 1
        row = [c.measurement.meter_type.meter_type, c.measurement.state, c.consumption, str(c.measurement.state_date), ]
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def meter_type_add(request):
    if request.method == "POST":
        form = MeterTypeForm(request.POST)
        if form.is_valid():
            meter_type = form.save(commit=False)
            meter_type.created_by = request.user
            meter_type.created_date = timezone.now()
            meter_type.save()
            return redirect('meters')
    else:
        form = MeterTypeForm()
    return render(request, 'meters_type_edit.html', {'form': form})


def meter_type_edit(request, slug):
    meter_type = get_object_or_404(MeterType, slug=slug)
    if request.method == "POST":
        form = MeterTypeForm(request.POST, instance=meter_type)
        if form.is_valid():
            meter_type = form.save(commit=False)
            meter_type.created_by = request.user
            meter_type.created_date = timezone.now()
            meter_type.save()
            return redirect('meters')
    else:
        form = MeterTypeForm(instance=meter_type)
    return render(request, 'meters_type_edit.html', {'form': form})


def meter_type_remove(request, slug):
    meter_type = get_object_or_404(MeterType, slug=slug)
    # Add option to remove measurements together with removing meter type !!
    if request.method == "POST":
        meter_type.delete()
        return redirect('meters')
    return render(request, 'meters_type_remove.html', {'meter_type': meter_type})

from django import forms

from .models import Measurement, MeterType


class MeasurementForm(forms.ModelForm):

    class Meta:
        model = Measurement
        fields = ('meter_type', 'state', 'state_date')


class MeterTypeForm(forms.ModelForm):

    class Meta:
        model = MeterType
        fields = ('meter_type', 'meter_description', 'meter_id', 'unit_of_measure')

# Generated by Django 2.0.7 on 2018-07-06 10:53

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('meters', '0002_auto_20180706_1242'),
    ]

    operations = [
        migrations.CreateModel(
            name='Measurement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.DecimalField(decimal_places=3, max_digits=10)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('meter_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='measurement', to='meters.MeterType')),
            ],
        ),
    ]

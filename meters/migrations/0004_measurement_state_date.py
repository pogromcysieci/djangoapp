# Generated by Django 2.0.7 on 2018-07-10 09:03

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meters', '0003_measurement'),
    ]

    operations = [
        migrations.AddField(
            model_name='measurement',
            name='state_date',
            field=models.DateField(default=datetime.date(2018, 7, 31)),
        ),
    ]

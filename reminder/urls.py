from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^reminder/$', views.reminder_list, name='reminder_list'),
    url(r'^reminder/add/$', views.reminder_add, name="reminder_add"),
    url(r'reminder/edit/(?P<pk>\d+)/$', views.reminder_edit, name="reminder_edit"),
    url(r'reminder/remove/(?P<pk>\d+)/$', views.reminder_remove, name="reminder_remove"),
]

from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from .models import ReminderType, Reminder
from .forms import ReminderForm
import datetime


def reminder_list(request):
    reminders = Reminder.objects.all()
    reminders_sorted = sorted(reminders, key=lambda t: t.get_reminder_date())
    today = datetime.datetime.now().date()
    reminders_month = Reminder.objects.filter(reminder_date__month=today.month)
    return render(request, 'reminder_list.html', {'reminders': reminders_sorted,
                                                  'reminders_month': reminders_month})


def reminder_add(request):
    if request.method == "POST":
        form = ReminderForm(request.POST)
        if form.is_valid():
            reminder = form.save(commit=False)
            reminder.created_by = request.user
            reminder.created_date = timezone.now()
            reminder.save()
            return redirect("reminder_list")
    else:
        form = ReminderForm()
    return render(request, 'reminder_edit.html', {'form': form})


def reminder_edit(request, pk):
    reminder = get_object_or_404(Reminder, pk=pk)
    if request.method == "POST":
        form = ReminderForm(request.POST, instance=reminder)
        if form.is_valid():
            reminder = form.save(commit=False)
            reminder.created_by = request.user
            reminder.created_date = timezone.now()
            reminder.save()
            return redirect('reminder_list')
    else:
        form = ReminderForm(instance=reminder)
    return render(request, 'reminder_edit.html', {'form': form})


def reminder_remove(request, pk):
    reminder = get_object_or_404(Reminder, pk=pk)
    if request.method == "POST":
        reminder.delete()
        return redirect('reminder_list')
    return render(request, 'reminder_remove.html', {'reminder': reminder})

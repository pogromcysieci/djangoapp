from django.contrib import admin
from .models import Reminder, ReminderType

admin.site.register(ReminderType)
admin.site.register(Reminder)

from django.db import models
from django.utils import timezone
import datetime


class ReminderType(models.Model):
    reminder_type = models.CharField(max_length=50)
    reminder_description = models.CharField(max_length=200,
                                         blank=True,
                                         null=True)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.reminder_type


class Reminder(models.Model):
    reminder_date = models.DateField()
    reminder_type = models.ForeignKey(ReminderType, related_name='reminder', on_delete=models.CASCADE)
    reminder_desc = models.CharField(max_length=255)
    calc_year = models.BooleanField(default=True)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.reminder_type) + " - " + str(self.reminder_desc)

    def get_age(self):
        if self.calc_year:
            return datetime.date.today().year - self.reminder_date.year
        else:
            return ""

    def get_reminder_date(self):
        today = datetime.date.today()
        date_to_remind = datetime.date(today.year, self.reminder_date.month, self.reminder_date.day)
        if today > date_to_remind:
            date_to_remind = datetime.date(today.year + 1, self.reminder_date.month, self.reminder_date.day)
        return date_to_remind

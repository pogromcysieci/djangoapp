from django import forms

from .models import Reminder


class ReminderForm(forms.ModelForm):

    class Meta:
        model = Reminder
        fields = ('reminder_date', 'reminder_type', 'reminder_desc', 'calc_year')